
(in-package #:cl-user)

(defpackage #:cgn-system
  (:use #:asdf #:cl))

(in-package #:cgn-system)

(defsystem cgn
  :version "008"
  :depends-on (#:ltk)
  :components ((:file "cgn")))
