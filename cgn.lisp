#| 
File:cgn.lisp 
Content: package for using gnuplot from Lisp 
Author: Felip Alaez Nadal 
Last update: 3-IV-2008 
Revision number: 116 |# 
 
#| 
 
    cgn 
    Copyright (C) Felip Alaez Nadal 2006-2008 
 
    This program is free software; you can redistribute it and/or modify 
    it under the terms of the GNU General Public License as published by 
    the Free Software Foundation; either version 2 of the License, or 
    (at your option) any later version. 
 
    This program is distributed in the hope that it will be useful, 
    but WITHOUT ANY WARRANTY; without even the implied warranty of 
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
    GNU General Public License for more details. 
 
    You should have received a copy of the GNU General Public License 
    along with this program; if not, write to the Free Software 
    Foundation, Inc., 51 Franklin Steet, Fifth Floor, Boston, MA  02111-1307  USA 
 
|# 
 
 
 
 
(declaim (optimize (speed 0) (safety 3) (debug 3) (compilation-speed 0))) 
 
(provide :cgn) 
 
(defpackage :cgn 
  (:use :cl :ltk) 
  (:export #:start-gnuplot #:format-gnuplot #:close-gnuplot #:set-title #:set-grid #:on #:off #:plot-function #:replot-function #:x #:y #:set-range #:plot-points #:postscript-copy #:w32 #:print-graphic #:plot #:plot-1  #:save-cgn #:load-cgn #:with-gnuplot #:animate-function #:enable-debug #:disable-debug #:show-release-info #:plot-scatter-graphic #:create-scatter-graphic-description #:create-data-serie #:splot #:plot-histogram #:plot-complex-histogram #:discrete) 
  (:documentation "cgn is a library to control gnuplot from inside Common Lisp.")) 
 
(in-package :cgn) 
 
 
(defun which-os ( ) 
  "
This is the function that CGN uses to guess which operative system is running. This looks for :win32 at *features*, if It finds It, we are using a Windows machine.
Returns 'w32 on a Windows machine, 'unix in other case." 
  (if (member :win32 *features* ) 
      'w32 
      'unix 
      ) 
  ) 
 
 
(defvar *default-connection-name* 'default-gnuplot-connection "The name of the default connection to use." ) 

(defparameter  *gnuplot* (make-hash-table ) "Gnuplot's connection") 

(defvar *os* 'linux "The Operative System used by the user. You should not bind It directly, use the with-gnuplot macro instead. ") 

(defvar *debug* nil "A flag controlling if cgn has to do debug.") 

(defvar *release* 009 "The number of release.") 

(defvar *release-date* "2/3/2008" ) 

(defvar *points* 100 "The number of points to plot a function.") 
 
 
 
 
;Something like '(connection name)!!! 
(defun format-gnuplot ( text  &rest arguments  ) 
  "Same as gformat, but adds a ~% at the end of the output." 
    (let
      (
       (nou_text (concatenate 'string text " ~%" ))
       )
    (apply #'gformat nou_text arguments )
    ) 
 
) 
 

(defun gformat (  text  &rest arguments) 
  "This is the hearth of CGN. This function is very similar to the format function, but It sends Its output to Gnuplot. You can use an arbitrary format string with this, and It will understand It. If you use something like '(connection connection-name) in Its argument list, gformat will send Its output to the gnuplot connection named connection-name."
  (let 
      ( 
       arguments_nets  
       connection 
       ) 
    (dolist ( arg arguments ) 
      (if (listp arg ) 
	  (progn 
	    (setq connection (second arg )) 
	    ) 
	  (setq arguments_nets (concatenate 'list arguments_nets (list arg )))) 
      ) 
    (unless connection 
      (setq connection *default-connection-name* )) 
    (multiple-value-bind ( conexio trobat ) (gethash connection *gnuplot* ) 
      (if trobat 
	  (progn 
	    (apply #'format  conexio text arguments_nets) 
	    (if *debug* 
		(progn 
		  (apply #'format t text arguments_nets ) 
		  )) 
	    (force-output conexio) 
	    ) 
	  (error "A connection named '~A' does not exist" connection ) 
	  ) 
      ) 
    ) 
(values) 
)
 

(defclass simple-data-serie () 
  ( 
   (x :accessor data-serie-x :initarg :x ) 
 
   (title :accessor data-serie-title :initarg :title ) 
   ) 
  ) 
 
(defclass data-serie-with-errors ( simple-data-serie) 
  ( 
   (y :accessor data-serie-y :initarg :y) 
   ( error_x :accessor data-serie-error_x :initarg :error_x ) 
   ( error_y :accessor data-serie-error_y :initarg :error_y ) 
   ) 
  ) 
 
 
(defclass graphic () 
  (				; "A struct to save all the data-series of a graphic." 
   (series :accessor graphic-series :initarg :series) 
   (title :accessor graphic-title :initarg :title ) 
   ) 
  ) 
 
(defclass histogram ( graphic ) 
  ( 
   (xtics :accessor histogram-xtics :initarg :xtics) 
   ) 
  ) 
 
(defclass scatter-graphic ( graphic ) 
  () 
  ) 

 
(defclass 3d-data-serie-with-errors ( data-serie-with-errors ) 
  ( 
   (z :accessor data-serie-z :initarg :z ) 
   (error_z :accessor data-serie-error_z :initarg :error_z ) 
   ) 
  ) 
 
(defgeneric create-scatter-graphic-description (title) ) 
 
(defmethod create-scatter-graphic-description ( title ) 
  "Creates a scatter-graphic struct to save information about the graphic." 
  (make-instance 'scatter-graphic :title title :series '() ) 
  ) 
 
(defgeneric create-histogram-description ( title xtics )) 
 
(defmethod create-histogram-description ( title xtics) 
  "Creates a scatter-graphic struct to save information about the graphic." 
  (make-instance 'histogram :title title  :xtics xtics :series '() ) 
  ) 
 
(defgeneric create-scatter-data-serie ( sc x y error_x error_y title )) 
 
(defmethod create-scatter-data-serie ( ( sc graphic ) x y error_x error_y  title) 
  "Creates a new serie on sc. Note that you must always give the errors. If you don't want errors, just set them to zero." 
  (setf (graphic-series sc) (concatenate 'list (graphic-series sc) (list (make-instance 'data-serie-with-errors :x x :y y :error_x error_x :error_y error_y :title title))))) 
 
(defgeneric create-scatter-3d-data-serie ( sc x y z error_x error_y error_z title )) 
(defmethod create-scatter-3d-data-serie ( ( sc graphic ) x y z error_x error_y error_z title ) 
  (setf (graphic-series sc) (concatenate 'list (graphic-series sc) (list (make-instance '3d-data-serie-with-errors :x x :y y :error_x error_x :error_y error_y :z z :error_z error_z :title title))))) 
 
(defgeneric create-histogram-data-serie ( hg x title ) ) 
 
(defmethod create-histogram-data-serie ( (hg histogram) x title ) 
  "Creates a new serie for a histogram. x is a list of values" 
  (setf (graphic-series hg ) (concatenate 'list (graphic-series hg ) (list (make-instance 'simple-data-serie :x x  :title title ))))) 
 
(defgeneric plot-scatter-graphic ( sc  &key fit-function )) 
 
;fit-function es una funcio de gnuplot (un string).
(defmethod plot-scatter-graphic ( (sc scatter-graphic )  &key fit-function) 
  "This method is used to plot a scatter graphic. sc is an object of type scatter-graphic-description, cointaining some data series which we want to plot. plot-scatter-graphic simply will plot each of them with Its name as legend title. fit-function is a key parameter used to provided a fit function to plot with the data series. At the moment, fit-function must be a string containing some function that gnuplot can understand." 
  (let 
      ( 
       (numero_de_serie 0) 
       (nombre_de_series (length (graphic-series sc ))) 
       (plot_command (if fit-function (format nil "plot ~A ," fit-function ) "plot ") )
	 ) 
    (if (= nombre_de_series 0 ) 
	(error "Can't plot an empty graphic.") 
	) 
    (if (= nombre_de_series 1 ) 
	(setq plot_command (concatenate 'string plot_command (format nil " '-' with xyerrorbars title '~A' " (data-serie-title (elt  (graphic-series sc ) 0)))) )
	(progn 
					;This works for doing plot '-' , '-' , '-' , '-' , '-' ... 
	  (do ((i 0 (incf i ))) 
	      ((= i (- nombre_de_series 1 )) t ) 
	    (if (= i 0 ) 
		(setq plot_command (concatenate 'string plot_command (format nil " '-' with xyerrorbars title '~A', " (data-serie-title (elt  (graphic-series sc ) i))))) 
		(setq plot_command (concatenate 'string plot_command (format nil " '-' with xyerrorbars title '~A', " (data-serie-title (elt  (graphic-series sc ) i))))) 
		) 
	    ) 
	  (setq plot_command  (concatenate 'string plot_command (format nil " '-' with xyerrorbars title '~A' " (data-serie-title (elt  (graphic-series sc ) (- nombre_de_series 1 )))))) 
	  ) 
	) 
    (format-gnuplot "set title '~A' " (graphic-title sc)) 
     
    (format-gnuplot plot_command ) 
    (dolist ( graphic (graphic-series sc )) 
      (let 
	  (  
	   (x (data-serie-x graphic)) 
	   (y (data-serie-y graphic)) 
	   (error_x (data-serie-error_x graphic)) 
	   (error_y (data-serie-error_y graphic)) 
	   ) 
	 
	(incf numero_de_serie ) 
	(do ((i 0 (incf i ))) 
	    ((= i (length x ) ) (values)) 
	  (format-gnuplot " ~F ~F ~F ~F~%" (elt  x i) (elt y i ) (elt  error_x i ) (elt  error_y i ))) 
	(format-gnuplot "e" ) 
	) 
      ) 
    ) 
  ) 
   
 
(defgeneric plot-complex-histogram ( hg   )) 
 
(defmethod plot-complex-histogram ( (hg histogram )  ) 
  "Plots an histogram." 
  (let* 
      ( 
       (xtic_command "set xtic ( " ) 
       
       (xtics (histogram-xtics hg )) 
       (nombre_de_xtics (length xtics )) 
        
       ) 
    ;In the future we must see if the number of bars is correct, but now we don't do this. 
    ;First we obtain the xtics. 
    (do ((i 0 (incf i ))) 
	((= i (- nombre_de_xtics 1) ) t ) 
      (setq xtic_command (concatenate 'string xtic_command (format nil "  '~A' ~D ,"  (elt  xtics  i) i))) 
      ) 
    (setq xtic_command (concatenate 'string xtic_command (format nil "  '~A' ~D ) "  (elt xtics  (- nombre_de_xtics 1 )) (- nombre_de_xtics 1 )))) 
    (format-gnuplot xtic_command ) 
     
 
    (format-gnuplot "set title '~A' " (graphic-title hg)) 
    (format-gnuplot "set style data histogram " ) 
    (format-gnuplot "set style histogram" ) 
    (format-gnuplot "set style fill solid border -1") 
					;(set-range 'y 0 (apply #'max data )) 
 
    (let 
	( 
	 (nombre_de_series (length (graphic-series hg ))) 
	 (plot_command "") 
	 ) 
      (if (= nombre_de_series 0 ) 
	  (error "Can't plot an empty graphic.") 
	  ) 
      (if (= nombre_de_series 1 ) 
	  (setq plot_command (format nil "plot '-' with   title '~A' " (data-serie-title (elt  (graphic-series hg ) 0)))) 
	  (progn 
					;Now the code to do plot '-' , '-' , '-' , '-' , '-' ... 
	    (do ((i 0 (incf i ))) 
		((= i (- nombre_de_series 1 )) t ) 
	      (if (= i 0 ) 
		  (setq plot_command (concatenate 'string plot_command (format nil "plot '-'  title '~A', " (data-serie-title (elt (graphic-series hg ) i))))) 
		  (setq plot_command (concatenate 'string plot_command (format nil " '-'  title '~A', " (data-serie-title (elt (graphic-series hg ) i))))) 
		  ) 
	      ) 
	    (setq plot_command  (concatenate 'string plot_command (format nil " '-'   title '~A' " (data-serie-title (elt (graphic-series hg ) (- nombre_de_series 1 )))))) 
	    ) 
	  ) 
      (format-gnuplot plot_command ) 
      ) 
     
    (dolist ( graphic (graphic-series hg )) 
      (let 
	  (  
	   (x (data-serie-x graphic)) 
	   ) 
	 
	(do ((i 0 (incf i ))) 
	    ((= i (length x ) ) (values)) 
	  (format-gnuplot " ~F " (elt  x i) ) 
	  ) 
	(format-gnuplot "e" ) 
	) 
 
     
       
      ) 
    ) 
  (format-gnuplot "reset" ) 
  ) 
  
 
(defun show-release-info () 
  "Shows the CGN version being used, plus Its release date." 
  (format t "Cgn ~A released ~A~%" *release* *release-date*) 
  (values) 
  ) 
 
(defun enable-debug () 
  "Call this function if you want to enable debug for CGN. Enabling debug will make CGN to show everything It sends to gnuplot through *standard-output*." 
  (setq *debug* t)) 
 
(defun disable-debug () 
  "Call this function to stop showing debug information." 
  (setq *debug* nil)) 
 
(defun add-new-gnuplot-connection ( name &key ( path "gnuplot" ) (persistent t)) 
  "This is the core of the new multiconnection CGN. 

  When you call this function, a new connection called name is created. Then you can refer to It using the with-gnuplot-connection name macro. 
  This function understands two key parameters:
  * ) path : is the path where the gnuplot executable is found. 
  * ) persistent : if this is set to t, the gnuplot windows won't be closed after calling close-gnuplot or delete-gnuplot-connection. This is used, for example, in the definition of the with-gnuplot macro.
"

  (multiple-value-bind ( contingut trobat ) (gethash name *gnuplot*) 
    (declare (ignore contingut )) 
    (if trobat 
	(error "Can't create connection named '~A', It already exists" name ) 
	(progn 
	  (let 
	      ( 
	       (options  (if persistent (list "-persist" ) nil )) 
	       )  
	    (setf (gethash name *gnuplot* )(do-execute path options )) 
	    ) 
	  ) 
	) 
    ) 
  (values) 
  ) 
    
 
(defun delete-gnuplot-connection ( name ) 
"Closes the gnuplot connection named name." 
  (multiple-value-bind ( name-connection trobat ) (gethash name *gnuplot*) 
    (if trobat 
	(progn 
	  (format name-connection "quit ~%" ) 
	  (force-output name-connection ) 
	  (remhash name *gnuplot* ) 
	  ) 
	(error "A connection named '~A' does not exist. " name ) 
	) 
    ) 
  (values) 
  ) 
 
(defun start-gnuplot (&key ( persistent nil) (path "gnuplot") )  
  "Runs gnuplot as a subprocess. This should be the first function to call." 
  (add-new-gnuplot-connection *default-connection-name* :persistent persistent :path path) 
  ) 
 
 
 
(defun close-gnuplot () 
  "Closes gnuplot" 
  (maphash (lambda ( key connection ) 
	     (declare (ignore key)) 
	     (format connection "quit ~%" ) 
	     (force-output connection ) 
	     ) 
	   *gnuplot* 
	   ) 
  (setf *gnuplot* (make-hash-table )) 
  (values) 
  ) 
 
(defun islambda ( l ) 
  "Returns t if l is a lambda list " 
  (if (listp l ) 
      (progn 
	(if (eq 'lambda (first l )) 
	    t) 
	) 
      nil 
      ) 
  ) 
 
(defun isdiscrete ( l ) 
  "Returns t if l is something like (discrete xx yy )" 
  (if (listp l ) 
      (progn 
	(if (eq 'discrete (first l )) 
	    t) 
	) 
      nil 
      ) 
  ) 
 
;This macro generates the plot and splot macros, avoiding code duplication. One problem to face in the future is that this doesn't write doc strings for them. 
(defmacro generate-plotting-macro ( name ) 
  "Generates the plot and splot macros." 
  `(defmacro ,name ( what-to-print &rest options ) 
    "The plot and splot macros integrate the CGN Gnuplot API. They provide an elegant and lispy way to print functions that gnuplot already know (lisp strings containing something like \"sin(x)\" or \"tan(x)*exp(x)\"). 
   plot and splot are very easy to use: just write (plot something gnuplot_options) or (splot something gnuplot_options). Something must be a string, containing a gnuplot function or the name of a file, enclosed between ''. gnuplot_options are lisp symbols, with the same name of the gnuplot option. Eg: if you want to set the title of the graph, you would use something like title 'my-graphic' in gnuplot. You will use just the same in CGN.
Here you can see some examples:
(plot \"x\" )
(plot \"    'my-data-file'    \" )
(plot \"sin(x)\" with lines)
(plot \"cos(x)*x\" title 'a simple graphic' with lines )
(splot \"x + y*tan(x*y)\" title 'something')


An easy way to begin using these two macros is running (cgn::plot-test) or (cgn::splot-test). They will show you some examples and their output.
"
     (let 
	 ( 
	  (cmd ',name ) 
	  ) 
       `(block nil 
	  (gformat "~( ~A ~) ~A" ',cmd ,what-to-print ) 
	  (dolist ( option (quote ,options )) 
	    (cond 
	      ((symbolp option ) ;if the argument is a symbol naming an option of gnuplot , we must convert it lowercase 
	    (gformat " ~( ~A  ~) " option ) ) 
	      ((stringp option ) 
	       (gformat " '~A' " option ) ;If It's a string must be treated as a literal, but we put ' ' around It. 
	       ) 
	      (t 
	       (gformat " ~A " option )) 
	      ) 
	    ) 
	  (gformat " ~%" ) 
	  (values) 
	  ) 
       ) 
    ) 
  ) 
 
 
 
;This way we generate the plot macro. 
(generate-plotting-macro plot ) 
 
;With this we generate the splot macro. 
(generate-plotting-macro splot ) 
 
 
(defun data-serie-from-function ( sc function xmin xmax title ) 
  "Adds a new data serie to sc." 
  (let 
      ( 
       llista_x  
       llista_y 
       llista_xerror 
       llista_yerror 
       (increment (/ (- xmax xmin) *points* ) ) 
       ) 
    (do ((variable_loop xmin (incf variable_loop increment ))) 
	((>= variable_loop xmax ) t ) 
      (setq llista_x (concatenate 'list llista_x (list variable_loop ) ) ) 
      (setq llista_y (concatenate 'list llista_y (list (funcall function variable_loop ) ))) 
      (push 0 llista_xerror ) 
      (push 0 llista_yerror ) 
      ) 
    (create-scatter-data-serie sc llista_x llista_y llista_xerror llista_yerror title) 
    ) 
  ) 
 
(defun data-serie-from-discrete ( sc lst title ) 
  (let
      (
       (elems (length lst ))
       )
    (cond 
      ((= elems 2 ) ;something like (discrete xy )
       (let 
	   ( 
	    (xy (second lst )) 
	    xx 
	    yy 
	    error_x 
	    error_y 
	    ) 
	 (dolist (punt xy ) 
	   (setq xx (concatenate 'list xx (list (first punt )))) 
	   (setq yy (concatenate 'list yy (list (second punt )))) 
	   (push 0 error_x ) 
	   (push 0 error_y ) 
	   ) 
	 (create-scatter-data-serie sc xx yy error_x error_y title) 
	 ) 
       ) 
      ((= elems 3 ) ;something like (discrete xx yy )
       (let* 
	   ( 
	    (xx (second lst )) 
	    (yy (third lst )) 
	    (l1 (length xx )) 
	    (l2 (length yy )) 
	    error_x 
	    error_y 
	    ) 
	 (if (not (= l1 l2 )) 
	     (error "The x and y series must have the same length." )) 
	 (do ((i 0 (incf i ))) 
	     ((= i l1 ) t ) 
	   (push 0 error_x ) 
	   (push 0 error_y ) 
	   ) 
	 (create-scatter-data-serie sc xx yy error_x error_y title) 
	 ) 
       )
      ((= elems 4 ) ;something like (discrete xx yy yerror )
       (let* 
	   ( 
	    (xx (second lst )) 
	    (yy (third lst )) 
	    (l1 (length xx )) 
	    (l2 (length yy )) 
	    error_x 
	    (error_y  (nth 3 lst ))
	    ) 
	 (if (not (= l1 l2 )) 
	     (error "The x and y series must have the same length." )) 
	 (do ((i 0 (incf i ))) 
	     ((= i l1 ) t ) 
	   (push 0 error_x ) 
	   ) 
	 (create-scatter-data-serie sc xx yy error_x error_y title) 
	 ) 
       )
      ((= elems 5 ) ;something like (discrete xx yy xerror yerror )
        (let* 
	   ( 
	    (xx (second lst )) 
	    (yy (third lst )) 
	    (l1 (length xx )) 
	    (l2 (length yy )) 
	    (error_x  (nth 3 lst ))
	    (error_y (nth 4 lst ))
	    ) 
	 (if (not (= l1 l2 )) 
	     (error "The x and y series must have the same length." )) 
	 (create-scatter-data-serie sc xx yy error_x error_y title) 
	 ) 
       )
      ) 
    )
  )
 
 (defun data-serie-from-string ( sc title )
   "This is a trick to print strings within a scatter graphic. We just let x as the string and then use the title field to store the function to print."
   (create-scatter-data-serie sc title nil nil nil title )
   )
 
(defun extract-property-from-list ( name options ) 
  "This is an auxiliar function used in plot2d and plot3d. It simply extracts the value of some property in a list like (property1 value1 property2 value2 ... ). This is used to parse the options for plot2d and plot3d." 
  (let 
      ( 
       resultat 
       ) 
    (dolist (option options ) 
      (if (eq name (first option )) 
	    (setq resultat option ) 
	    ) 
      ) 
    resultat 
    ) 
  ) 
 
 

(defun plot-everything-from-plot2d ( sc xrange options  ) 
  "This is an auxiliar function which plot2d calls. You shouldn't never call this directly, please use plot2d instead."
  (let* 
      ( 
       (cmd (format nil  "plot [ ~F : ~F ]  " (second xrange ) (third xrange ) )) 
       (series (graphic-series sc )) 
       (nombre_de_scatters (length series )) 
       (yrange (extract-property-from-list 'y options )) 
       (legend (extract-property-from-list 'legend options )) 
       (xlabel (extract-property-from-list 'xlabel options )) 
       (ylabel (extract-property-from-list 'ylabel options )) 
       (logx (extract-property-from-list 'logx options )) 
       (logy (extract-property-from-list 'logy options )) 
       (box (extract-property-from-list 'box options )) 
       ;Options from gnuplot 
       (gnuplot_term (extract-property-from-list 'gnuplot_term options )) 
       (gnuplot_out_file (extract-property-from-list 'gnuplot_out_file options )) 
       (gnuplot_pm3d (extract-property-from-list 'gnuplot_pm3d options )) 
       (gnuplot_preamble (extract-property-from-list 'gnuplot_preamble options )) 
       (gnuplot_dumb_term_command (extract-property-from-list 'gnuplot_dumb_term_command options )) 
       (gnuplot_ps_term_command (extract-property-from-list 'gnuplot_ps_term_command options )) 
       (gnuplot_default_term_command (extract-property-from-list 'gnuplot_default_term_command options )) 
       ;Nom de la conexio a usar 
       ( connection (extract-property-from-list 'connection options )) 
       ) 
     
    (cond 
      (connection 
       (setq connection (second connection ))) 
      (t 
       (setq connection *default-connection-name* )) 
      ) 
     
    (if yrange 
	(setq cmd (concatenate 'string cmd (format nil " [ ~F : ~F ] " (second yrange ) (third yrange ) ))) 
	) 
    (if legend  
	(progn 
	  (do ((i 0 (incf i ))) 
	      ((= i (- (length legend ) 1)) t) 
	    (setf (data-serie-title (elt  series i ) ) (elt  legend ( + i 1) ) ) 
	    ) 
	  ) 
	) 
    (if xlabel 
	(format-gnuplot  "set xlabel '~A' " (second xlabel ) (list 'connection connection )   ) ) 
    (if ylabel  
	(format-gnuplot  "set ylabel '~A' " (second ylabel )  (list 'connection connection ))) 
    (if logx  
	(format-gnuplot "set logscale x"  (list 'connection connection ))) 
    (if logy  
	(format-gnuplot "set logscale y"  (list 'connection connection ))) 
    ;In the future we must improve the [box false] option. CGN only sets the border option to zero. Maxima also erases xtics and ytics, but we must watch how to do that. 
    (if box  
	(if (eq (second box ) 'false ) 
	    (format-gnuplot "set border 0"  (list 'connection connection )))) 
    (if gnuplot_term  
	(format-gnuplot "set term ~A " gnuplot_term (list 'connection connection  ))) 
    (if gnuplot_out_file  
	(format-gnuplot "set output '~A' " gnuplot_out_file  (list 'connection connection ))) 
    (if gnuplot_pm3d 
	(if (eq (second gnuplot_pm3d ) 'true ) 
	    (format-gnuplot "set pm3d" (list 'connection connection  )))) 
    (if gnuplot_preamble 
	(format-gnuplot gnuplot_preamble  (list 'connection connection ))) 
    (if gnuplot_dumb_term_command 
	(format-gnuplot gnuplot_dumb_term_command (list 'connection connection ))) 
    (if gnuplot_ps_term_command  
	(format-gnuplot gnuplot_ps_term_command (list 'connection connection ))) 
    (if gnuplot_default_term_command  
	(format-gnuplot gnuplot_default_term_command (list 'connection connection ))) 
    (cond 
      ((= nombre_de_scatters 1 ) 
       (if (stringp (data-serie-x (nth 0 series ) ) )
	   (setq cmd (concatenate 'string cmd "  " (data-serie-x (elt series 0 ) ) "  title ' " (data-serie-title (elt series 0 )) " ' "))
	   (setq cmd (concatenate 'string cmd " '-' title '" (data-serie-title (elt  series 0 ) ) "' with xyerrorbars " )) 
	   )
       )
      ((> nombre_de_scatters 1 ) 
       (dotimes  ( n (- (length series ) 1 ) )
	 (if (stringp (data-serie-x (nth n series )))
	     (setq cmd (concatenate 'string cmd "  " (data-serie-x (elt series n ) ) "  title ' " (data-serie-title (elt series n )) " ' , "))
	     (setq cmd (concatenate 'string cmd " '-' title '" (data-serie-title (elt series n )) "' with xyerrorbars, " )) 
	     )
	 )
       (if (stringp (data-serie-x (nth (- nombre_de_scatters 1 ) series ) ) )
	   (setq cmd (concatenate 'string cmd "  " (data-serie-x (elt series (- nombre_de_scatters 1 )) ) "  title ' " (data-serie-title (elt series (- nombre_de_scatters 1) )) " '  "))
	   (setq cmd (concatenate 'string cmd " '-' title '" (data-serie-title (elt  series (- nombre_de_scatters 1))) "' with xyerrorbars" )) 
	   )
       ) 
      ) 
    (format-gnuplot cmd (list 'connection connection)) 
    (dolist ( serie series ) 
      (let 
	  ( 
	   (x (data-serie-x serie )) 
	   (y (data-serie-y serie )) 
	   (errorx (data-serie-error_x serie ))
	   (errory (data-serie-error_y serie ))
	   ) 
	(if (not (stringp x ))
	    (progn 
	      (do ((i 0 (incf i ))) 
		  ((= i (length x )) t ) 
		(format-gnuplot "  ~F ~F ~F ~F " (elt  x i ) (elt  y  i) (elt errorx i) (elt errory i) (list 'connection connection )) 
		) 
	      (format-gnuplot "e" (list 'connection connection )) 
	      )
	    )
	) 
      ) 
    )
  (format-gnuplot "reset" )
  ) 
 
;Una de les opcions de plot2d no ve de Maxima: es (connection nom-conexio). Serveix per a tindre multiples conexions alhora. 
(defun plot2d ( what-to-plot xrange   &rest options ) 
  "plot2d belongs to the new CGN api. This api tries to be very similar to the api from Maxima. 

  plot2d should be very easy to use for those who already know Maxima: if you have a Maxima plot2d() command to plot something, you just have to change [] by () and quote the name of the options and you'll get a valid CGN command. 

 plot2d can plot scatter plots and lisp functions (also lambda functions) and lists containing lisp functions and scatter plots. 

To plot an scatter graphic, you must use a list with the format (discrete xy) (discrete xx yy ) (discrete xx yy errory ) or (discrete xx yy errorx errory ).

 plot2d understands some options:

* connection : the name of the connection to use for plotting. This is not a Maxima options, It's something added from CGN to take benefits from the multiple connections avaliable since CGN009.
* y : the yrange to plot. When you use this option, you must provide a list containing ( 'y y_min y_max ) . 
* legend : this is an option to give a name to each data serie in the plot. You just provide strings. CGN will use the first string as the name for the first data serie and so on. If there are more data series than strings, CGN will give a default name to each of them. 
* xlabel : this is a string to put as the name for the x axis.
* ylabel : this is a string to put as the name for the y axis.
* logx : if you use this, the x axis will be shown in a logarithmic scale.
* logy : if you use this, the y axis will be shown in a logarithmic scale.
* box : this erases the box around the plot. 
* gnuplot_term_options
* gnuplot_out_file_options
* gnuplot_pm3d_options
* gnuplot_ps_term_command
* gnuplot_default_term_command

Please, try (cgn::plot2d-test) for a fast beggining.
" 
  (let 
      ( 
       (sc (create-scatter-graphic-description "Graphic" )) 
       (x0 (second xrange )) 
       (x1 (third xrange )) 
       (nombre_de_serie 1 ) 
       ) 
     
    (cond 
       
      ((functionp what-to-plot ) ;Si ens passen una funcio, plotegem una funcio. 
       (data-serie-from-function sc what-to-plot x0 x1 (format nil " ~A "  what-to-plot )) 
       )
      ((stringp what-to-plot )
       (data-serie-from-string sc what-to-plot )
       )
      ((listp what-to-plot ) ;Aquest es el mes complicat: cal veure que representa la llista i aixo es llarg i tedios de fer 
       (cond 
	 ((isdiscrete what-to-plot ) ;Si el primer element de la llista es 'discrete, ja esta clar que tenim un plot discret 
	  (data-serie-from-discrete sc what-to-plot (format nil "DataSerie~F" nombre_de_serie)) 
	  (incf nombre_de_serie ) 
	  ) 
	 (t ;En cas contrari, tenim una llista de plots de diferents tipus. Aci ve el cacau. 
	  (dolist ( variable_loop what-to-plot ) 
	    (cond 
	      ((listp variable_loop ) ;Si tenim una llista, ha de ser algo del tipus (discrete xx yy ) o (discrete xy ) o (parametric ...) 
	       (cond 
		 ((isdiscrete variable_loop ) 
		  (data-serie-from-discrete sc variable_loop (format nil "DataSerie~A" nombre_de_serie)) 
		  (incf nombre_de_serie ) 
		  ) 
		 )) 
	      ((functionp variable_loop) 
	       (data-serie-from-function sc variable_loop x0 x1 (format nil "~A" variable_loop )) 
	       )
	      ((stringp variable_loop )
	       (data-serie-from-string sc variable_loop ))
	      ) 
	    ) 
	  ) 
	 ) 
        
       ) 
      ) 
    (plot-everything-from-plot2d  sc xrange options ) 
    ) 
  ) 
 
 
;plot3d only can plot one function. It also understands the connection option. 
(defun plot3d ( function-to-plot xrange yrange  &rest options) 
  "plot3d belongs to the new CGN api. This api tries to be very similar to the api from Maxima. 

  plot3d should be very easy to use for those who already know Maxima: if you have a Maxima plot3d() command to plot something, you just have to change [] by () and quote the name of the options and you'll get a valid CGN command. 

 plot3d can plot scatter plots and lisp functions (also lambda functions) and lists containing lisp functions and scatter plots. 

 plot3d understands some options:

* connection : the name of the connection to use for plotting. This is not a Maxima options, It's something added from CGN to take benefits from the multiple connections avaliable since CGN009.
* z : the yrange to plot. When you use this option, you must provide a list containing ( 'z z_min z_max ) . 
* legend : this is an option to give a name to each data serie in the plot. You just provide strings. CGN will use the first string as the name for the first data serie and so on. If there are more data series than strings, CGN will give a default name to each of them. 
* xlabel : this is a string to put as the name for the x axis.
* ylabel : this is a string to put as the name for the y axis.
* logx : if you use this, the x axis will be shown in a logarithmic scale.
* logy : if you use this, the y axis will be shown in a logarithmic scale.
* box : this erases the box around the plot. 
* gnuplot_term_options
* gnuplot_out_file_options
* gnuplot_pm3d_options
* gnuplot_ps_term_command
* gnuplot_default_term_command

Please, try (cgn::plot3d-test) for a fast beggining."
  (let* 
      ( 
       (x0 (second xrange )) 
       (x1 (third xrange )) 
       (y0 (second yrange )) 
       (y1 (third yrange )) 
       (deltax (/ (- x1 x0) *points* )) 
       (deltay (/ (- y1 y0) *points* )) 
       (cmd (format nil "splot [~F : ~F] [ ~F : ~F ] '-' " x0 x1 y0 y1 )) 
       (zrange (extract-property-from-list 'z options )) 
       (legend (extract-property-from-list 'legend options )) 
       (xlabel (extract-property-from-list 'xlabel options )) 
       (ylabel (extract-property-from-list 'ylabel options )) 
       (zlabel (extract-property-from-list 'zlabel options )) 
       (logx (extract-property-from-list 'logx options )) 
       (logy (extract-property-from-list 'logy options )) 
       (box (extract-property-from-list 'box options )) 
					;Options from gnuplot 
       (gnuplot_term (extract-property-from-list 'gnuplot_term options )) 
       (gnuplot_out_file (extract-property-from-list 'gnuplot_out_file options )) 
       (gnuplot_pm3d (extract-property-from-list 'gnuplot_pm3d options )) 
       (gnuplot_preamble (extract-property-from-list 'gnuplot_preamble options )) 
       (gnuplot_dumb_term_command (extract-property-from-list 'gnuplot_dumb_term_command options )) 
       (gnuplot_ps_term_command (extract-property-from-list 'gnuplot_ps_term_command options )) 
       (gnuplot_default_term_command (extract-property-from-list 'gnuplot_default_term_command options )) 
       ) 
     
    (if zrange 
	(setq cmd (concatenate 'string cmd (format nil " [ ~F : ~F ] " (second yrange ) (third yrange ) ))) 
	) 
    (if legend  
	(setq cmd (concatenate 'string cmd (format nil " title '~A' " (second legend ) )))) 
    (if xlabel 
	(format-gnuplot "set xlabel '~A' " (second xlabel ) ) ) 
    (if ylabel  
	(format-gnuplot "set ylabel '~A' " (second ylabel ))) 
    (if zlabel  
	(format-gnuplot "set zlabel '~A' " (second zlabel ))) 
    (if logx  
	(format-gnuplot "set logscale x" )) 
    (if logy  
	(format-gnuplot "set logscale y" )) 
    ;In the future we must improve the [box false] option. CGN only sets the border option to zero. Maxima also erases xtics and ytics, but we must watch how to do that. 
    (if box  
	(if (eq (second box ) 'false ) 
	    (format-gnuplot "set border 0" ))) 
    (if gnuplot_term  
	(format-gnuplot "set term ~A " gnuplot_term )) 
    (if gnuplot_out_file  
	(format-gnuplot "set output '~A' " gnuplot_out_file )) 
    (if gnuplot_pm3d 
	(if (eq (second gnuplot_pm3d ) 'true ) 
	    (format-gnuplot "set pm3d" ))) 
    (if gnuplot_preamble 
	(format-gnuplot gnuplot_preamble )) 
    (if gnuplot_dumb_term_command 
	(format-gnuplot gnuplot_dumb_term_command )) 
    (if gnuplot_ps_term_command  
	(format-gnuplot gnuplot_ps_term_command )) 
    (if gnuplot_default_term_command  
	(format-gnuplot gnuplot_default_term_command )) 
    (format-gnuplot cmd ) 
    (do ((x x0 (incf x deltax ))) 
	((> x x1) t ) 
      (do ((y y0 (incf y deltay ))) 
	  ((> y y1 ) t ) 
	(format-gnuplot " ~F ~F ~F " x y (funcall function-to-plot x y ) ) 
	) 
      ) 
    (format-gnuplot "e" ) 
    ) 
   
  ) 
 
 
(defun update (  ) 
  "Refreshes the screen. This should not be called by the user." 
  (format-gnuplot "replot")) 
 
(defun set-title ( text) 
  "Sets the window title" 
  (cond  
    ((eq *gnuplot* nil) ) 
    ( t 
     (format-gnuplot  "set title '~A'" text) 
       
     )) 
  (values)) 
 
(defun set-grid (on-off  ) 
  "Activates/deactivates the grid." 
  (cond  
    ( (eq *gnuplot* nil)) 
    ( t 
     (cond 
       ((eq on-off 'on)  
	(format-gnuplot "set grid") 
	  
	) 
       ((eq on-off 'off)  
	(format-gnuplot "unset grid") 
	  
	) 
       (t (format t "I don't understand~%"))) 
     )) 
  (values)) 
 
 
(defun plot-function (funcio) 
  "Shows a function. It can show functions like f(x) (2D representation) or g(x,y) (3D representation). Tries to guess what you want to do." 
  (let  
      ( (variables nil) 
       parametric_2D 
       parametric_3D 
	(nombre_de_comes 0 ) 
	) 
    (dolist (letter (coerce funcio 'list)) 
      (cond 
	((eq letter #\,) (incf nombre_de_comes ) ) 
	((eq letter #\t) (setq parametric_2D t ) ) 
	((eq letter #\u) (setq parametric_3D t ) ) 
	((eq letter #\y) (setq variables t)))) 
    (cond 
      ((= nombre_de_comes 2 ) ;With 2 comma, parametric 3D 
       (splot funcio :parametric t)) 
      ((= nombre_de_comes 1 ) ;With 1 comma, parametrid 2d 
       (plot-1 funcio :parametric t))      
      ( t ;If none, normal mode plotting 
       (if variables 
	   (splot  funcio) 
	   (plot-1 funcio) 
	   ) 
       ) 
      ) 
    (values) 
    ) 
  ) 
 
(defun replot-function (funcio  ) 
  "Shows a n-variable function on gnuplot / replot version. '" 
  (let  
      ( (variables nil) 
       parametric_2D 
       parametric_3D 
	(nombre_de_comes 0 ) 
	) 
    (dolist (letter (coerce funcio 'list)) 
      (cond 
	((eq letter #\, ) (incf nombre_de_comes )) 
	((eq letter #\t) (setq parametric_2D t ) ) 
	((eq letter #\u) (setq parametric_3D t ) ) 
	((eq letter #\y) (setq variables t)))) 
    (if (not (= nombre_de_comes 0)) 
	(format-gnuplot  "set parametric; replot ~A ; unset parametric ;" funcio) 
	(format-gnuplot  "~%replot ~A " funcio)) 
    (values))) 
 
 
(defun enable-surface ( ) 
  "Enables the surface plotting, using the pm3d option." 
  (format-gnuplot "set pm3d") 
  ) 
 
(defun disable-surface (  ) 
  "Disables the surface plotting" 
  (format-gnuplot "unset pm3d" ) 
  ) 
 
(defun plot-1 ( string-to-plot &key parametric surface   ) 
  "Plots a function like f(x), only one variable. If parametric is t, you must give f(t),g(t) as input. If surface is t, you get a surface plotting." 
  (if surface 
      (enable-surface  ) 
      ) 
  (if parametric 
      (format-gnuplot "set parametric ; plot ~A ; unset parametric ; " string-to-plot ) 
      (format-gnuplot "plot ~A " string-to-plot ) 
      ) 
  (if surface 
      (disable-surface  ) 
      ) 
  ) 
 
(defun splot-1 ( string-to-plot &key parametric surface  ) 
  "Plots a function like f(x,y). If parametric is t, you must give f(u,v),g(u,v),p(u,v) as input.  If surface is t, you get a surface plotting." 
  (if surface 
      (enable-surface  ) 
      ) 
  (if parametric 
      (format-gnuplot "set parametric ; splot ~A ; unset parametric ; " string-to-plot ) 
      (format-gnuplot "splot ~A " string-to-plot ) 
      ) 
  (if surface 
      (disable-surface  ) 
      ) 
  ) 
 
(defun animate-function (  function &key variable initial_value increment number_of_frames fps   ) 
  "Animates function, changing the value of variable in increments of increment at velocity fps, until number_of_frames is reached. At the moment, only one paramether is permited." 
  (let  
      (  
       (contador initial_value) 
       (valor_final (+ initial_value (* number_of_frames increment ))) 
       ) 
     
    (loop 
       (format-gnuplot " ~A = ~A " variable contador )  
       (if (< contador valor_final ) 
	   (plot-function function ) 
	   (return) 
	   ) 
       (incf contador increment ) 
					;Here we can use the sleep lisp function or the pause gnuplot command. I use pause for portability, but this way the REPL isn't blocked as gnuplot plays the animation. If you then send something to gnuplot, that will be ignored until It ends. 
       (format-gnuplot "pause ~F~%" (coerce   (/ 1 fps ) 'float)) 
       ;(sleep (/ 1 fps )  ) 
       )) 
  (values) 
  ) 
     
 
(defun set-range ( eix min max  ) 
  "Sets the x or y range " 
  (cond 
    ((eq eix 'x) 
     (format-gnuplot  "set xrange [~F:~F]" min max) 
     ) 
    ((eq eix 'y) 
     (format-gnuplot  "set yrange [~F:~F]" min max) 
     ) 
    (t (format t "I don't understand~%"))) 
  (values)) 
 
 
(defun plot-points ( x y &key x_error y_error   ) 
  "This functions shows scatter plots, with x and y errors if desired" 
  (cond  
    ((eq x nil) ; Error 
     (format t "I can't plot a serie without x values~%") 
     (return-from plot-points (values))) 
    ((eq y nil) ; Error 
     (format t "I can't plot a serie without y values~%") 
     (return-from plot-points (values))) 
    ( t ;We can do the plot. Now treat the errors. 
     (cond 
       ((and x_error y_error) ;x and y error bars 
	(let ( 
	      (l_x (length x)) 
	      (l_y (length y)) 
	      (l_ey (length y_error)) 
	      (l_ex (length x_error)) 
	      len) 
	  (setq len (min l_x l_y l_ey l_ex)) 
	  (format-gnuplot "plot '-' with xyerrorbars") 
	  (do ((i 0 (incf i))) 
	      ((= i len) t) 
	    (format-gnuplot "~F        ~F      ~F      ~F  ~%" (elt  x i ) (elt  y i) (elt x_error i) (elt y_error i)) 
	    )) 
	(format-gnuplot "e") 
	) 
       ( x_error ;Only x error bars 
	(let ( 
	      (l_x (length x)) 
	      (l_y (length y)) 
	      (l_ex (length x_error)) 
	      len) 
	  (setq len (min l_x l_y l_ex)) 
	  (format-gnuplot "plot '-' with xerrorbars" ) 
	  (do ((i 0 (incf i))) 
	      ((= i len ) t) 
	    (format-gnuplot "~F        ~F      ~F~%" (elt  x i ) (elt y i) (elt x_error i)) 
	    )) 
	(format-gnuplot "e") 
	) 
        
       ( y_error ; Only y errorbars 
	(let ( 
	      (l_x (length x)) 
	      (l_y (length y)) 
	      (l_ey (length y_error)) 
	      len) 
	  (setq len (min l_x l_y l_ey)) 
	   
	  (format-gnuplot "plot '-' with yerrorbars" ) 
	  (do ((i 0 (incf i))) 
	      ((= i len ) t) 
	    (format-gnuplot "~F        ~F      ~F~%" (elt x i ) (elt y i) (elt y_error i)) 
	    ) 
	  (format-gnuplot "e" ) 
	  )) 
       ( t ; Without error bars 
	(let ( 
	      (l_x (length x)) 
	      (l_y (length y)) 
	      len) 
	  (setq len (min l_x l_y)) 
	  (format-gnuplot "plot '-' " ) 
	  (do ((i 0 (incf i))) 
	      ((= i len ) t) 
	    (format-gnuplot "~F        ~F  ~%" (elt x i ) (elt y i)) 
	    ) 
	  (format-gnuplot "e" ) 
	  ) 
	) 
       )) 
    ) 
  ) 
 
 
 
 
(defun postscript-copy ( filename  ) 
  "Saves a postscript copy of the screen ." 
  (format-gnuplot "set terminal postscript color") 
  (format-gnuplot  "set output '~F'" filename) 
  (format-gnuplot "set terminal pop") 
  (values)) 
 
 
 
(defun print-graphic ( &optional (os (which-os))   ) 
  "Sends the graphic to the printer. This function is os-dependent. It will try to guess which operative system is running. If you don't get a printed graphic, maybe the os type is being wrongly guessed. Then set the os parameter to 'w32 if you are using Windows." 
  (cond 
    ((eq os 'w32) ;Windows 
     (format-gnuplot "screendump")) 
    ( t ;Unix-like systems 
     (postscript-copy "tmp001.ps"  ) 
     (format-gnuplot "!lpr -p tmp001.ps") 
     (format-gnuplot "!rm -f tmp001.ps") 
     )) 
  (values) 
  ) 
 
 
(defun save-cgn ( file  ) 
  "This saves the gnuplot session to a file in order to restore It later.

You must understand that CGN doesn't save nothing. It's gnuplot who saves Its state. With the generated file you can restore the gnuplot state using (load-cgn \"file\" ), but you won't restore the CGN state."
  (format-gnuplot  "save '~A'" file) 
  (values)) 
 
(defun load-cgn (file  ) 
  "This restores the gnuplot state, loading a file previously saved with save-cgn. 

You must understand that CGN doesn't load anything. It just restores the gnuplot state, not CGN state."
  (format-gnuplot  "load '~A'" file) 
  (values)) 
 
 
(defun max-in-seq (seq)
  "Auxiliar function used to replace the previous (apply #'max list ), which doesn't work with vectors."
  (let ((max 0))
    (dotimes (i (length seq))
      (if (> (elt seq i) max)
	  (setf max (elt seq i))))
    max))
     


(defun plot-histogram (  xtics data ) 
  "This is a very simple function to plot histograms. Simply xtics is a list with the titles for the tics on the x axis, data is a list containing the data to plot. Warning: this function makes a reset after showing Its plot, to exit from the histogram mode of gnuplot. This is an undesirable side effect, but this function makes lots of changes to the styles in gnuplot." 
  (let 
      ( 
       (xtic_command "set xtic ( " ) 
       (nombre_de_barres (length data )) 
       (nombre_de_tics (length xtics )) 
       ) 
    (if (not (= nombre_de_barres nombre_de_tics )) 
	(error "The number of xtics is not equal to the number of bars. Please, correct this and try this function again.") 
	) 
    ;Now the xtics 
    (do ((i 0 (incf i ))) 
	((= i (- nombre_de_barres 1) ) t ) 
      (setq xtic_command (concatenate 'string xtic_command (format nil "  '~A' ~D ,"  (elt xtics i ) i))) 
      ) 
    (setq xtic_command (concatenate 'string xtic_command (format nil "  '~A' ~D ) "  (elt xtics (- nombre_de_barres 1 ) ) (- nombre_de_barres 1 )))) 
    (format-gnuplot xtic_command ) 
  
    (format-gnuplot "set style data histogram " ) 
    (format-gnuplot "set style histogram" ) 
    (format-gnuplot "set style fill solid border -1") 
    (format-gnuplot "set xtics nomirror rotate by -45" )
    (set-range 'y 0 (max-in-seq data)) 
    ;Now we draw the bars 
    (format-gnuplot "plot '-' " ) 
    (dotimes (i (length data)) 
      (format-gnuplot "~F" (elt data i)) 
      ) 
    (format-gnuplot "e") 
    ) 
  (format-gnuplot "reset" ) 
  ) 
 
(defmacro with-gnuplot ( (  &key os  (path "gnuplot") )  
			&body body) 
  "Creates a new gnuplot  , and evaluates body.
 
   This macro takes care of always having a running connection when you try to comunicate with gnuplot,  
   and to close the connection when you finish using It. Also binds the *os* special variable to the os paramether 
   in order to not having to pass It to the methods that are os-dependents  (at the moment, only print-graphic). You 
   can too specify the path where gnuplot resides ( pgnuplot.exe, on Windows ). This can be useful if you don't have 
   gnuplot at the path. This is a typical Windows problem. 
 
 
   This should be the prefered way to use cgn. But you can continue opening and closing the connection manually. Just  
   keep using (start-gnuplot) and (close-gnuplot) for that. I don't recommend to do that. 
 
   E.g : supose you want to print cos(x)*atan ( exp (y)) on a linux machine. You should use : 
    
 
      (with-gnuplot (  ) 
 
	    (plot-function \"cos(x)* atan (exp(y)) \" ) 
 
	    (print-graphic)) 
 
 Note that gnuplot works different in Windows. There the -persist option is ignored. Due to that, this macro works differently: you have to write something and push INTRO to exit from the macro. If we don't do this, the gnuplot window is closed immediately after terminating the macro. This is a little problem, but I think It's the best we can do. 
" 
  `(unwind-protect  
	(progn 
	  (let 
	      ( 
	       (*os* (if ,os (quote ,os ) (which-os ))) 
	       ) 
	    (start-gnuplot :persistent t :path ,path) 
	    ,@body 
		(if (eq *os* 'w32 ) (read )) 
	    (values) 
	    ) 
	  ) 
     (if *gnuplot* 
	 (close-gnuplot) 
	 (values) 
	 ) 
     ) 
  ) 
 
 
(defun easy-plotter () 
  "This is a little utility function to use plot. 

When you call (cgn::easy-plotter) you get a ltk window where you can put the name of the function to plot and the parameters you want to use. You can plot data files from there, too. Simply put It's name between enclosing '', like \"   'my-data-file.txt'  \" ."
  (with-ltk () 
    (let* 
	( 
	 (window (make-instance 'frame :master nil :width 400 :height 200 )) 
	 (function_label (make-instance 'label :master window :text "Function or file to draw" ) ) 
	 (options_label (make-instance 'label :master window :text "Options for the plot" ) ) 
	 (function_entry (make-instance 'entry :master window :text "sin(x)" )) 
	 (options_entry (make-instance 'entry :master window :text "with lines" ) ) 
	 (ok_button (make-instance 'button :master window :text "Plot It!"  
				   :command 
				   (lambda () 
				     (let 
					 ( 
					  (function (text function_entry )) 
					  (string_options (text options_entry )) 
					  options 
					  (funcioneta '(lambda () ) ) 
					  ordre_plot 
					  ) 
				       (with-input-from-string  ( stream   string_options ) 
					 (loop 
					    (let 
						( 
						 (valor (read stream nil 'eof  ) ) 
						 ) 
					      (if (eq valor 'eof ) 
						  (progn 
						    (return) 
						    ) 
						     
						  ) 
					      (setq options (concatenate 'list options (list valor ))) 
					      ) 
					    ) 
					 ) 
				       (setq ordre_plot  (list (concatenate 'list (list 'plot function )  options  ) )) 
				       (setq funcioneta (concatenate 'list funcioneta ordre_plot )) 
				       (with-gnuplot ( ) 
					 (funcall (eval funcioneta )) 
					 ) 
				       ) 
				     ) 
				   )) 
				        
	 ) 
      (pack window ) 
      (pack function_label ) 
      (pack function_entry ) 
      (pack options_label ) 
      (pack options_entry ) 
      (pack ok_button ) 
      ) 
    ) 
  ) 
 
(defun plot2d-test () 
  "Runs a little example for plot2d." 
  (with-gnuplot () 
    (format t "plot2d is the new (from cgn009) plotting facility for cgn. You should think of plot2d as the plot2d command from Maxima: just write your Maxima code, lispify It (changing [] for () , erasing commas and quoting symbols ) and run It inside lisp. ~%") 
    (format t "plot2d can plot lisp and lambda functions. Example: (plot2d #'sqrt '(x 0 10 ) ) ~%" ) 
    (plot2d #'sqrt '(x 0 10 ) ) 
    (format t "Write 'n' <<return>> for the next plot. ~%" ) 
    (read ) 
    (format t "It can plot discrete plots too. Simply do something like (plot2d '(discrete (1 2 3 4 5 6 7 8 9 10 ) (1.1 2.5 4.21 2.12 3.15 4.57 6.87 8.74 12.5 13.4789 ) ) '(x 0 10 ) ) ~%" ) 
    (plot2d '(discrete (1 2 3 4 5 6 7 8 9 10 ) (1.1 2.5 4.21 2.12 3.15 4.57 6.87 8.74 12.5 13.4789 ) ) '(x 0 10 ) ) 
    (format t "Write 'n' <<return>> for the next plot. ~%" ) 
    (read ) 
    (format t "Of course, you can plot multiple functions and discrete plots on the same graphic. Just use a list as a paramether, like (plot2d (list #'sqrt '(discrete (1.5 2.2 3.4 4.7 ) (0.12 3.45 4.78 10.1 ) ) #'cos ) '(x 0 10 ) ) ~%" ) 
    (plot2d (list #'sqrt '(discrete (1.5 2.2 3.4 4.7 ) (0.12 3.45 4.78 10.1 ) ) #'cos ) '(x 0 10 ) ) 
    (format t "Write 'n' <<return>> for the next plot. ~%" ) 
    (read ) 
    (format t "plot2d understands the same paramethers as plot2d from Maxima. As an example, see this:  (plot2d (lambda (x ) (* (cos x ) (sqrt x ) ) ) '(x 0 10 ) '(y -10 10 ) '(xlabel \"Time (s)\" ) '(ylabel \"Tachyon flux ( particles/cm**2 )\") '(legend \"Tachyon flux as a function of time.\" ) )") 
    (plot2d (lambda (x ) (* (cos x ) (sqrt x ) ) ) '(x 0 10 ) '(y -10 10 ) '(xlabel "Time (s)" ) '(ylabel "Tachyon flux ( particles/cm**2 )") '(legend "Tachyon flux as a function of time." ) )
    (format t "Write 'n' <<return>> for the next plot. ~%" ) 
    (read ) 
    (format t "Plot2d also understands gnuplot functions. You just use strings to name them. See this : (plot2d (list '(discrete ( 1 2 3 4 5 6) ( 2 3 4 5 6 7 ) ) \" 2*x + 1\" ) '(x 0 9 ) '(legend \"Data from experiment\" \"Theoretical fit\" ) ) ~%" )  
    (plot2d (list '(discrete ( 1 2 3 4 5 6) ( 1.8 3.15 4.1 4.9 6.2 6.9 ) ) " x + 1" ) '(x 0 9 ) '(legend "Data from experiment" "Theoretical fit" )) 
    
    ) 
  ) 
 
(defun plot-test () 
  (with-gnuplot () 
    (format t "plot is a macro that lispifies the syntax of the plot command from gnuplot. This is part of the cgn's gnuplot api. Just use plot as you would use the equivalent plot command in gnuplot. Note that the plot macro can only draw one plot at time. ~%" ) 
    (format t "Write 'n' <<return>> for the next plot. ~%" ) 
    (read ) 
    (format t "Take a look at these examples : (plot \"sin(x)\" with lines ) ~%" ) 
    (plot "sin(x)" with lines ) 
    (format t "Write 'n' <<return>> for the next plot. ~%" ) 
    (read ) 
    (format t "(plot \"cos(x) * sin(x) \" title \"An oscillating function\" )~%" ) 
    (plot "cos(x) * sin(x) " title "An oscillating function" ) 
    (format t "You can also use the plot macro to plot something from a file. Just put '' around the file name, like in (plot \" 'mydatafile.dat' \" title \" Data from experiment \" ) " ) 
    )) 
 
(defun splot-test () 
  (with-gnuplot () 
    (format t "splot is a macro that lispifies the syntax of the splot command from gnuplot. This is part of the cgn's gnuplot api. Just use splot as you would use the equivalent splot command in gnuplot. Note that the splot macro can only draw one plot at time. ~%" ) 
    (format t "Write 'n' <<return>> for the next plot. ~%" ) 
    (read ) 
    (format t "(splot \"cos(x) * exp(y) \" title \" A beautiful plot \" ) " ) 
    (splot "cos(x) * exp(y) " title "A beautiful plot " ) 
    ) 
  ) 
 
(defmacro with-gnuplot-connection ( name &body body) 
  "This macro lets you use the new multiconnection features from CGN. Just give the name of the connection to use and you will send everything under this macro to that." 
  `(let 
       ( 
	(*default-connection-name* ,name ) 
	) 
     ,@body ) 
  ) 
 
 
(defun multiple-connection-test () 
  (with-gnuplot () 
    (format t "There are more than one way to use multiple gnuplot connections in CGN. The first one is to use the macro with-gnuplot-connection name . Just create some connections and use them." ) 
    (format t "As an example, you can run  
(with-gnuplot () 
   
    (add-new-gnuplot-connection 'test ) 
    (add-new-gnuplot-connection 'window ) 
    (add-new-gnuplot-connection 'new ) 
    (with-gnuplot-connection 'test  
      (plot \"sin(x)\" title \"This is the test connection\" ) 
      ) 
    (with-gnuplot-connection 'window 
      (plot \"tan(x)\" title \"This is the window connection\" ) 
      ) 
    (with-gnuplot-connection 'new 
      (plot \"exp(x)\" title \"This is the new connection\" ) 
      ) 
    (plot \"cos(x)*sin(x)\" title \"This is the default connection\" ) 
    ) 
") 
  (format t "Write 'n' <<return>> for the next plot. ~%" ) 
    (read ) 
 (add-new-gnuplot-connection 'test ) 
    (add-new-gnuplot-connection 'window ) 
    (add-new-gnuplot-connection 'new ) 
    (with-gnuplot-connection 'test  
      (plot "sin(x)" title "This is the test connection" ) 
      ) 
    (with-gnuplot-connection 'window 
      (plot "tan(x)" title "This is the window connection" ) 
      ) 
    (with-gnuplot-connection 'new 
      (plot "exp(x)" title "This is the new connection" ) 
      ) 
    (plot "cos(x)*sin(x)" title "This is the default connection" ) 
    ) 
(format t "Another easy way to use multiple connections is to use the connection option in plot2d and plot3d. You  just give the name of the connection to use as a parameter.") 
(format t "As an example, try to run this: 
(with-gnuplot () 
  (add-new-gnuplot-connection 'connection2 ) 
  (plot2d #'sqrt '(x 0 10 ) '(connection connection2 ) '(legend \"This is connection2\" )) 
  (plot2d #'sin '(x 0 10 )) 
  )~%" ) 
(format t "Write 'n' <<return>> for the next plot. ~%" ) 
(read ) 
(with-gnuplot () 
  (add-new-gnuplot-connection 'connection2 ) 
  (plot2d #'sqrt '(x 0 10 ) '(connection connection2 ) '(legend "This is connection2" )) 
  (plot2d #'sin '(x 0 10 )) 
  ) 
)


(defun plot-points-and-function ( x y func xstart xend data-title func-title) 
  "This function is used to plot a x-y data serie plus a function which is supposed to match. x and y are sequences ( lists or vectors) with the same length, func is a string containing a gnuplot function, xstart and xend is the range to use in the x axis , data-title and func-title are the titles to show in the graphic for the data series and the function. This function has been contributed by Dan Becker."
  (cond  
    ((eq x nil) ; Error 
     (format t "I can't plot a serie without x values~%") 
     (return-from plot-points-and-function (values))) 
    ((eq y nil) ; Error 
     (format t "I can't plot a serie without y values~%") 
     (return-from plot-points-and-function (values))) 
    ( t ;We can do the plot. Now treat the errors. 
     (cond 
       ( t ; Without error bars 
	(let ( 
	      (l_x (length x)) 
	      (l_y (length y)) 
	      len) 
	  (setq len (min l_x l_y)) 
	  (format-gnuplot "plot [~F : ~F] ~A title \"~A\", '-' title \"~A\""
			  xstart xend func func-title data-title)
	  (do ((i 0 (incf i))) 
	      ((= i len ) t) 
	    (format-gnuplot "~F        ~F  ~%" (elt x i ) (elt y i)) 
	    ) 
	  (format-gnuplot "e" ) 
	  ) 
	) 
       )) 
    ) 
  ) 


(defun plot-points-and-function-test ()
  (format t " plot-points-and-function is used to plot a x-y data serie plus a function which is supposed to match. x and y are sequences ( lists or vectors) with the same length, func is a string containing a gnuplot function, xstart and xend is the range to use in the x axis , data-title and func-title are the titles to show in the graphic for the data series and the function. This function has been contributed by Dan Becker. ~%" )
  (with-gnuplot ()
    (format t "As an example, you can see the output from (plot-points-and-function '( 1 2 3 4 5 6 7 8 9 10 ) '( 1.97 3.12 3.94 5.2 5.99 7.03 8 9.05 10 11.2 ) \" x + 1 \"  0 10 \"Cost evolution in time\" \"Prevision (2008)\" )" )
    (plot-points-and-function '( 1 2 3 4 5 6 7 8 9 10 ) '( 1.97 3.12 3.94 5.2 5.99 7.03 8 9.05 10 11.2 ) " x + 1 "  0 10 "Cost evolution in time" "Prevision (2008)" )
    )
  )


(defun maxim ( llista )
  (let
      (
       ( maxim (first llista ))
       )
    (dolist ( item llista )
      (if (> item maxim) 
	  (setq maxim item )
	  )
      )
    maxim
    )
  )

(defun minim ( llista )
    (let
      (
       ( minim (first llista ))
       )
    (dolist ( item llista )
      (if (< item minim) 
	  (setq minim item )
	  )
      )
    minim
    )
  )

(defun classificar_en_histogrames_amb_eficiencia( vector min max h )
  (let*
      (
       ( N (ceiling (/  (- max min) h )))
       (resultats (make-array N :initial-element 0 ))
       bin
       eficiencia
       )
    (dolist ( numeret vector )
      (if (and (<= numeret max ) (>= numeret min )) ;Ens assegurem de tindre 1 < x < 10
	  (progn
	    (setq bin (floor (- numeret min ) h ))
	    (if (>= bin max ) (decf bin ))
	    (setq eficiencia (/ (- numeret 1 ) 9)) 
	    (incf (elt resultats bin ) eficiencia )
	    )
	  )
      )
    resultats
    )
  )

(defun classificar_en_histogrames_per_bin ( vector min max h )
  (let*
      (
       bin
       ( elements (ceiling (/ (- max min ) h )))
       ( resultats (make-array elements :initial-element 0))
       )
    (dolist ( numeret (coerce vector 'list ))
      (if (< numeret max )
	  (progn
	    (setq bin (floor  (/ (- numeret min) h )))
	    (if (>= bin elements ) (decf bin ))
	    (incf (elt resultats bin ) )
	    )
	  )
      )
    (values resultats  )
    )
  )