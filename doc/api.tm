<TeXmacs|1.0.6>

<style|generic>

<\body>
  <with|font-base-size|20|<strong|Opcions de la funci� plot2d de
  Maxima<with|font-base-size|20|>>>

  <\with|font-base-size|12>
    Les opcions sempre tenen el format [nom valors ]. Valors sol ser un o
    varios valors senzills (strings, nombres) que en ocasions seran llistes.

    \;

    [legend titol1 titol2 ... titoln ] dona els t�tols per a cadascuna de les
    s�ries que es mostren al gr�fic. S'ha de tradu�r en qu� quan fem plot
    algunacosa, posem plot algunacosa title titol1 , plot altracosa title
    titol2 ...

    [xlabel titol ] posa el t�tol de l'eix x. Es tradueix en set xlabel titol

    [ylabel titol ] posa el t�tol de l'eix y. Es tradueix en set ylabel titol

    \;

    [logx] . Posa l'eix x en escala logar�tmica. Es tradueix en set logscale
    x.

    [logy] . Posa l'eix y en escala logar�tmica. Es tradueix en set logscale
    y.

    \;

    [box false]. Aquesta opci� lleva el marge al voltant del gr�fic.

    \;

    [style]. �s un poc m�s llarga d'implementar, usar llistes com a
    par�metres. Canvia com es mostren algunes coses.

    \;

    [grid, gridx, gridy] \ Posa el nombre de punts que s'han d'usar en una
    graella xy. gridx �s el nombre de punts en la graella per la x i gridy el
    nombre de punts en direcci� y.

    \;

    [transform_xy ] crec que no t� sentit en cgn.
  </with>

  \;

  <with|font-base-size|14|<strong|Opcions per al gnuplot:>>

  <\with|font-base-size|12>
    Aquestes opcions no deurien ocasionar problemes perqu� solen ser les
    ordres literals que se li passen al gnuplot, aix� que no cal
    preprocessar-les en general.

    gnuplot_term Tipus de terminal que usar� el gnuplot.

    gnuplot_out_file Nom de l'arxiu on es guardar� la sortida de gnuplot.

    gnuplot_pm3d Activa i desactiva el m�dul pm3d.

    [gnuplot_preamble string] Inserta ordres al gnuplot abans de mostrar el
    gr�fic. D'aquesta forma, per exemple, es podrien fer histogrames.

    [gnuplot_curve_titles ] Controla els t�tols que s'usen en dibuixar algo
    usant plot.

    gnuplot_curve_styles Controla l'estil de les l�nies.

    gnuplot_default_term_command

    gnuplot_dumb_term_command

    gnuplot_ps_term_command �s l'ordre que se li passa al gnuplot per a
    enxufar la terminal de postScript.\ 
  </with>
</body>

<\initial>
  <\collection>
    <associate|language|spanish>
  </collection>
</initial>